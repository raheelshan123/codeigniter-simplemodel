<?php
if (!defined('BASEPATH'))	exit('No direct script access allowed');

require_once SYSDIR . '/core/Model.php';

class SimpleModel extends CI_Model implements Iterator
{
	protected $table = NULL;
	protected $primary = NULL;
	protected $class;
	protected $position;
	protected $results = array();
	protected $results_count = 0;
	public $old_primary = NULL;
	
	function __construct($values = NULL)
	{
		if (get_called_class() === __CLASS__)
			return;
		$this->class = $class = get_called_class();
		if (!isset($this->table) || !is_string($this->table)) {
			$this->table = $this->guess_tablename();
		}
		if (!isset($this->primary) || !is_string($this->primary)) {
			$default_key   = 'id';
			$this->primary = $default_key;
			if (!isset($this->$default_key)) {
				$this->$default_key = NULL;
			}
		}
		if ($values !== NULL) {
			foreach ($values as $key => $value) {
				$this->$key = $value;
			}
		}
	}
	
	function __toString()
	{
		$return = "model ({$this->class}) : ";
		foreach ($this->get_my_properties() as $property => $value) {
			$return .= "[{$property} = {$value}] ";
		}
		return trim($return);
	}
	
	protected function process_results($query)
	{
		$class = get_called_class();
		if ($query->num_rows() === 0) {
			return new $class();
		}
		$this->results = array();
		foreach ($query->result() as $result) {
			$this->results[] = new $class($result);
		}
		foreach ($query->row() as $key => $value) {
			$this->$key = $value;
		}
		$this->results_count = $query->num_rows();
		$query->free_result();
	}
	
	public function get_my_properties()
	{
		$ref    = new ReflectionObject($this);
		$pros   = $ref->getProperties(ReflectionProperty::IS_PUBLIC);
		$result = array();
		foreach ($pros as $pro) {
			false && $pro = new ReflectionProperty();
			$result[$pro->getName()] = $pro->getValue($this);
		}
		return $result;
	}
	
	public function guess_tablename()
	{
		$class = preg_replace('/(_m|_model)?$/i', '', get_class($this));
		get_instance()->load->helper('inflector');
		return plural(strtolower($class));
	}
	
	public function build_db_object($properties_to_save)
	{
		$db_object = new stdClass();
		$key       = $this->primary;
		if (!empty($properties_to_save) && is_array($properties_to_save)) {
			if (isset($this->$key) && !empty($this->$key) && !in_array($key, $properties_to_save)) {
				array_push($properties_to_save, $key);
			}
			foreach ($properties_to_save as $field) {
				if (isset($this->$field))
					$db_object->$field = $this->$field;
			}
		} else {
			foreach ($this->get_my_properties() as $property => $value) {
				$db_object->$property = $value;
			}
		}
		return $db_object;
	}
	
	public static function find($column, $value, $limit = NULL)
	{
		$class  = get_called_class();
		$return = new $class();
		$return->where($column, $value);
		if ($limit !== NULL) {
			$return->limit($limit);
		}
		$return->get();
		return $return;
	}
	
	public static function query()
	{
		$class  = get_called_class();
		$return = new $class();
		return $return;
	}
	
	public static function all()
	{
		$class  = get_called_class();
		$return = new $class();
		$return->get();
		return $return;
	}
	
	public static function count()
	{
		$class  = get_called_class();
		$return = new $class();
		return $return->count_all();
	}
	
	public function set_primary($value)
	{
		$key               = $this->primary;
		$this->old_primary = $this->$key;
		$this->$key        = $value;
	}
	
	public function create($properties_to_save = array(), $last_insert_id = TRUE)
	{
		$db_object = $this->build_db_object($properties_to_save);
		$key       = $this->primary;
		if ($return = $this->db->insert($this->table, $db_object)) {
			if ($last_insert_id === TRUE) {
				$this->$key = $this->db->insert_id();
			}
			return $return;
		}
		return FALSE;
	}
	
	public function save($properties_to_save = array())
	{
		$db_object = $this->build_db_object($properties_to_save);
		$key       = $this->primary;
		if (property_exists($db_object, $key) && !empty($db_object->$key)) {
			if (is_null($this->old_primary)) {
				$this->db->where($key, $db_object->$key);
			} else {
				$this->db->where($key, $this->old_primary);
			}
			return $this->db->update($this->table, $db_object);
		}
		return FALSE;
	}
	
	public function remove()
	{
		$key = $this->primary;
		if (isset($this->$key) && !empty($this->$key)) {
			return $this->where($key, $this->$key)->delete();
		}
		return FALSE;
	}
	
	public function result($offset = NULL)
	{
		if (count($this->results) === 0) {
			return FALSE;
		}
		if ($offset !== NULL && is_int($offset)) {
			if (array_key_exists($offset, $this->results)) {
				return $this->results[$offset];
			}
			return FALSE;
		}
		return $this->results;
	}
	
	public function to_json()
	{
		return json_encode($this);
	}
	
	public function select($fields = '*', $backticks = TRUE)
	{
		$this->db->select($fields, $backticks);
		return $this;
	}
	
	public function select_max($field, $rename = NULL)
	{
		$this->db->select_max($field, $rename);
		return $this;
	}
	
	public function select_min($field, $rename = NULL)
	{
		$this->db->select_min($field, $rename);
		return $this;
	}
	
	public function select_avg($field, $rename = NULL)
	{
		$this->db->select_avg($field, $rename);
		return $this;
	}
	
	public function select_sum($field, $rename = NULL)
	{
		$this->db->select_sum($field, $rename);
		return $this;
	}
	
	public function distinct()
	{
		$this->db->distinct();
		return $this;
	}
	
	public function where($field, $value = NULL)
	{
		$this->db->where($field, $value);
		return $this;
	}
	
	public function or_where($field, $value)
	{
		$this->db->or_where($field, $value);
		return $this;
	}
	
	public function where_in($field, $values)
	{
		$this->db->where_in($field, $values);
		return $this;
	}
	
	public function or_where_in($field, $values)
	{
		$this->db->or_where_in($field, $values);
		return $this;
	}
	
	public function where_not_in($field, $values)
	{
		$this->db->where_not_in($field, $value);
		return $this;
	}
	
	public function or_where_not_in($field, $values)
	{
		$this->db->or_where_not_in($field, $value);
		return $this;
	}
	
	public function like($field, $match, $use_wildcards = 'both')
	{
		$this->db->like($field, $match, $use_wildcards);
		return $this;
	}
	
	public function or_like($field, $match, $use_wildcards = 'both')
	{
		$this->db->or_like($field, $match, $use_wildcards);
		return $this;
	}
	
	public function not_like($field, $match, $use_wildcards = 'both')
	{
		$this->db->not_like($field, $match, $use_wildcards);
		return $this;
	}
	
	public function or_not_like($field, $match, $use_wildcards = 'both')
	{
		$this->db->or_not_like($field, $match, $use_wildcards);
		return $this;
	}
	
	public function group_by($fields)
	{
		$this->db->group_by($fields);
		return $this;
	}
	
	public function order_by($field, $order = 'ASC')
	{
		$this->db->order_by($field, $order);
		return $this;
	}
	
	public function join($table, $condition, $type = 'left')
	{
		$this->db->join($table, $condition, $type);
		return $this;
	}
	
	public function limit($limit, $offset = 0)
	{
		$this->db->limit($limit, $offset);
		return $this;
	}
	
	public function get()
	{
		try {
			$query = $this->db->get($this->table);
			$this->process_results($query);
			return $this;
		}
		catch (exception $ex) {
			return FALSE;
		}
	}
	
	public function delete()
	{
		return $this->db->delete($this->table);
	}
	
	public function count_all()
	{
		return $this->db->count_all($this->table);
	}
	
	public function rewind()
	{
		$this->position = 0;
	}
	
	public function current()
	{
		return $this->results[$this->position];
	}
	
	public function key()
	{
		return $this->position;
	}
	
	public function next()
	{
		$this->position++;
	}
	
	public function valid()
	{
		return $this->position < count($this->results);
	}
}